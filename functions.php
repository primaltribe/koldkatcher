<?php
function add_google_fonts() {
    wp_enqueue_style( 'google-fonts', 'https://fonts.googleapis.com/css?family=Ubuntu:400,300,500', false );
}
add_action( 'wp_enqueue_scripts', 'add_google_fonts' );


// Add Bootstrap Styles
function add_bootstrap() {
    wp_enqueue_style( 'animate_css', get_stylesheet_directory_uri() . '/css/animate.css', false );
    wp_enqueue_style( 'bootstrap_css', get_stylesheet_directory_uri() . '/css/bootstrap.css', false );
    wp_enqueue_script( 'bootstrap_js', get_stylesheet_directory_uri() . '/js/bootstrap.js', false );
    wp_enqueue_script( 'wow_js', get_stylesheet_directory_uri() . '/js/wow.js', false );
    wp_enqueue_style( 'glyphs_eot', get_stylesheet_directory_uri() . '/fonts/glyphicons-halflings-regular.eot', false );
    wp_enqueue_style( 'glyphs_svg', get_stylesheet_directory_uri() . '/fonts/glyphicons-halflings-regular.svg', false );
    wp_enqueue_style( 'glyphs_ttf', get_stylesheet_directory_uri() . '/fonts/glyphicons-halflings-regular.ttf', false );
    wp_enqueue_style( 'glyphs_woff', get_stylesheet_directory_uri() . '/fonts/glyphicons-halflings-regular.woff', false );
    wp_enqueue_style( 'glyphs_woff2', get_stylesheet_directory_uri() . '/fonts/glyphicons-halflings-regular.woff2', false );
}

add_action( 'wp_enqueue_scripts', 'add_bootstrap' );

add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
function theme_enqueue_styles() {
    wp_enqueue_style( 'divi-style', get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'divi-child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array('divi-style')
    );
}

add_action( 'init', 'create_post_type' );
function create_post_type() {
    register_post_type( 'footer-item',
        array(
            'labels' => array(
                'name' => __( 'Footer Items' ),
                'singular_name' => __( 'Footer Item' )
            ),
            'public' => true,
            'has_archive' => true,
        )
    );
}



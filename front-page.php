<!DOCTYPE html>
<!--[if IE 6]>
<html id="ie6" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 7]>
<html id="ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html id="ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<?php elegant_description(); ?>
	<?php elegant_keywords(); ?>
	<?php elegant_canonical(); ?>

	<?php do_action( 'et_head_meta' ); ?>

	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

	<?php $template_directory_uri = get_template_directory_uri(); ?>
	<!--[if lt IE 9]>
	<script src="<?php echo esc_url( $template_directory_uri . '/js/html5.js"' ); ?>" type="text/javascript"></script>
	<![endif]-->

	<script type="text/javascript">
		document.documentElement.className = 'js';
	</script>

	<?php wp_head(); ?>
</head>


<body <?php body_class(); ?>>

<div class="row bg-holder">
	<img class="wow bounceInDown center animated" src="<?php echo get_stylesheet_directory_uri(); ?>/images/kold-katcher-logo.png" />
		<nav class="front navbar navbar wow animated fadeInLeft">
			<?php
				$menuClass = 'nav';
				if ( 'on' == et_get_option( 'divi_disable_toptier' ) ) $menuClass .= ' et_disable_top_tier';
				$primaryNav = '';

				$primaryNav = wp_nav_menu( array( 'theme_location' => 'primary-menu', 'container' => '', 'fallback_cb' => '', 'menu_class' => $menuClass, 'menu_id' => 'top-menu', 'echo' => false ) );

				if ( '' == $primaryNav ) :
					?>

					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar"><span class="glyphicon glyphicon-menu-hamburger" aria-hidden="true"></span></button>
<!--						<a class="navbar-brand" href="#">Project name</a>-->
					</div>
					<div id="navbar" class="collapse navbar-collapse">
					<ul id="front" class="<?php echo esc_attr( $menuClass ); ?>">

							<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>" ><?php esc_html_e( 'Home', 'Divi' ); ?></a></li>

						<?php show_page_menu( $menuClass, false, false ); ?>
						<?php show_categories_menu( $menuClass, false ); ?>
					</ul>
						</div>
					<?php
				else :
					echo( $primaryNav );
				endif;
			?>
		</nav>
	<div class="clr"></div>
</div>

<?php wp_footer(); ?>
<?php get_footer(); ?>

</body>